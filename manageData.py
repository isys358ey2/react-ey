import quandl
from neo4j import GraphDatabase

driver = GraphDatabase.driver("bolt://localhost:7687", auth=("neo4j", "supersecure"))

quandl.ApiConfig.api_key = '2_o5WKi-zYZDEoYWy2rw'

with driver.session() as session:
    result = session.run("MATCH (s:Stock) RETURN s")
    for record in result:
        quandlData = quandl.get(record["s"]["quandlCode"], returns="numpy")
        for row in quandlData:
            session.run(f"MATCH (s:Stock {{code: \"{record['s']['code']}\"}}) CREATE (s)-[:hasData]->(n:Data {{date: localdatetime(\"{row[0]}\"), price: {row[1]}, volume: {row[2]}}})")

