const expect = require('chai').expect;
const request = require('request');

it ('Main page sends correct status', (done) => {
    request('http://localhost:3000' , function(error, response, body) {
        expect(response.statusCode).to.equal(200);
        done();
    });
});

it ('should proxy correctly', (done) => {
    request('http://localhost:3000/api/ping', function (error, response, body) {
        expect(response.statusCode).to.equal(200);
        done();
    })
});
