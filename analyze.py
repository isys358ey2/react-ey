import os
import sys
from neo4jrestclient.client import GraphDatabase
import quandl
from datetime import date, timedelta
import statistics
from numpy import polyfit

quandl.ApiConfig.api_key = os.environ["QUANDL_API_KEY"]
db = GraphDatabase("http://localhost:7474", username=os.environ["NEO4J_USERNAME"], password=os.environ["NEO4J_PASSWORD"])

dateRanges = ["week", "month", "year"]

if len(sys.argv) != 2:
    print("Must have a date range to look over.") 
    exit(1)

if sys.argv[1] not in dateRanges:
    print("Date range must be one of the following: " + str(dateRanges))
    exit(1)

if sys.argv[1] == "week":
    dayLength = 7
elif sys.argv[1] == "month":
    dayLength = 30
else:
    dayLength = 365

stocksQuery = db.query("MATCH (s:Stock) RETURN s.code")

stocks = {}
for stock in stocksQuery.elements:
    stockCode = stock[0]
    stocks[stockCode] = {}
    endDateString = db.query(f"MATCH (s:Stock) WHERE s.code = \"{stockCode}\" RETURN s.endDate", returns=(str)).elements[0][0]
    endDate = date(int(endDateString[0:4]), int(endDateString[5:7]), int(endDateString[8:10]))
    startDate = endDate - timedelta(dayLength)
    startDateString = f"{startDate.year}-{startDate.month}-{startDate.day}"
    stockData = db.query(f"MATCH (s:Stock)-->(d:Data) WHERE s.code = \"{stockCode}\" AND d.dataDate > date(\"{startDateString}\") RETURN d.dataDate, d.price, d.volume")
    priceList = list(map(lambda x: x[1], stockData.elements))
    stocks[stockCode]['linearFit'] = polyfit(range(len(priceList)), priceList, 1)
    stocks[stockCode]['slope'] = (stocks[stockCode]['linearFit'][1] - stocks[stockCode]['linearFit'][0]) / stocks[stockCode]['linearFit'][1]
    stocks[stockCode]['percentChange'] = (stocks[stockCode]['slope'] - 1) * 100
    stocks[stockCode]['sd'] = statistics.stdev(priceList)
    stocks[stockCode]['variance'] = statistics.variance(priceList)


for stock in stocks:
    print(f"{stock}:")
    for key, value in stocks[stock].items():
        print(f"\t{key}: {value}")