import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import './App.css';

// Pages
import Login from './pages/Login.page';
import Dashboard from "./pages/Dashboard.page";
import ManagePortfolios from "./pages/ManagePortfolios.page";
import ManageUsers from "./pages/MangeUsers.page";
import PortfolioSVE from "./pages/PortfolioSVE.page";
import NotFound from "./pages/NotFound.page";

class App extends Component {
  render() {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={Dashboard}/>
                <Route exact path="/portfolios" component={ManagePortfolios}/>
                <Route path="/portfolio/:id" component={PortfolioSVE}/>
                <Route path="/users" component={ManageUsers}/>
                <Route path="/login" component={Login}/>
                <Route component={NotFound}/>
            </Switch>
        </BrowserRouter>
    );
  }
}

export default App;
