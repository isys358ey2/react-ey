import React, {Component} from 'react';
import Header from "../components/Header.component";

export default class NotFound extends Component {
    render() {
        return (
            <div>
                <Header/>
                <h1>
                    404 - Page not found!
                </h1>
            </div>

        )
    }
}