import React, {Component} from 'react';
import Header from "../components/Header.component";
import PortfolioSummaryList from "../components/PortfolioSummaryList.component";

export default class ManageProfiles extends Component {
    render() {
        return (
            <div>
                <Header/>
                <PortfolioSummaryList/>
            </div>
        )
    }
}