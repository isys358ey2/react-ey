import React, { Component } from 'react';
import {
    Container,
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    Form,
    InputGroup,
    Input,
    InputGroupAddon,
    FormGroup,
    Button
} from 'reactstrap';
export default class Login extends Component {
    render() {
        return (
            <Container>
                <Row className="mt-5 justify-content-center">
                    <Col sm="6">
                        <Card>
                            <CardHeader>EYSS - Log in</CardHeader>
                            <CardBody>
                                <Form>
                                    <InputGroup>
                                        <InputGroupAddon addonType="prepend">@</InputGroupAddon>
                                        <Input placeholder="email" autoComplete="username" name="username"/>
                                    </InputGroup>
                                    <br/>
                                    <FormGroup>
                                        <Input type="password" autoComplete="current-password" placeholder="password" name="password"/>
                                    </FormGroup>
                                    <FormGroup>
                                        <Button color="primary" className="float-right">Submit</Button>
                                    </FormGroup>
                                </Form>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </Container>
        )
    }
}