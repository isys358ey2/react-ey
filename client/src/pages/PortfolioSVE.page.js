import React, {Component} from 'react';
import {
    Container,
    Row,
    Col,
    Card,
    CardBody,
    CardHeader,
    Table,
} from 'reactstrap';

import Header from '../components/Header.component';
import MarketView from '../components/MarketView.component';

export default class PortfolioSVE extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            portfolio: {},
        }
    }

    componentWillMount() {
        fetch("/api/portfolio/" + this.props.match.params.id).then((response) => {
            return response.json()
        }).then((result) => {
            this.setState({
                isLoaded: true,
                portfolio: result,
            })
        }).catch((error) => {
            this.setState({
                error,
                isLoaded: true,
            })
        })
    }

    render() {
        const { error, isLoaded, portfolio } = this.state;
        if (error) {
            return (
                <div>
                    <Header/>
                    <h1>Error: {error}</h1>
                </div>
            )
        } else if(!isLoaded) {
            return (
                <div/>
            )
        } else {
            const fname = this.state.portfolio[0]['p.fname'];
            const lname = this.state.portfolio[0]['p.lname'];
            const principal = this.state.portfolio[0]['p.principal'];
            return <div>
                <Header/>
                <Container fluid>
                    <Row className="m-4 row-eq-height">
                        <Col xs={4}>
                            <Card>
                                <CardHeader>Portfolio</CardHeader>
                                <CardBody>
                                    First Name: {fname}
                                    <br/>
                                    Last Name: {lname}
                                    <br/>
                                    Principal: {principal}
                                    <br/>
                                    <Table>
                                        <thead>
                                        <tr>
                                            <th>Code</th>
                                            <th>Name</th>
                                            <th>Amount</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {portfolio.map((p, i) => (
                                            <tr key={i}>
                                                <td>{p['s.code']}</td>
                                                <td>{p['s.name']}</td>
                                                <td>{p['h.amount']}</td>
                                            </tr>
                                        ))}
                                        </tbody>
                                    </Table>
                                </CardBody>
                            </Card>
                        </Col>
                        <Col xs={8}>
                            <MarketView portfolio={portfolio}/>
                        </Col>
                    </Row>
                    <Row className="m-4">
                        <Col>
                            <Card>
                                <CardHeader>Recommendations</CardHeader>
                                <CardBody></CardBody>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>
        }
    }
}