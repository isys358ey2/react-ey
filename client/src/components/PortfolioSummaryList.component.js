import React, {Component} from 'react';
import {
    Container,
    Card,
    CardBody,
    InputGroup,
    InputGroupAddon,
    Input
} from 'reactstrap';
// Components
import PortfolioSummary from "./PortfolioSummary.component";

export default class PortfolioSummaryList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            portfolios: [],
            filteredPortfolios: [],
        }
    }

    componentDidMount() {
        fetch("/api/portfolios").then((response) => {
            return response.json();
        }).then((result) => {
            this.setState({
                isLoaded: true,
                portfolios: result,
                filteredPortfolios: result,
            })
        }).catch((error) => {
            this.setState({
                error,
                isLoaded: true,
            })
        })
    }

    filterPortfolios(event) {
        const filterText = event.target.value;
        this.setState({
            filteredPortfolios: this.state.portfolios.filter((portfolio) => {
                return portfolio.p.properties.lname.toLowerCase().includes(filterText.toLowerCase()) ||
                    portfolio.p.properties.fname.toLowerCase().includes(filterText.toLowerCase());
            })
        })
    }

    render() {
        const { error, isLoaded, filteredPortfolios } = this.state;
        if(error) {
            return (
                <div>
                    Error: {error}
                </div>
            )
        } else if(!isLoaded) {
            // Handle loading case
            return(
                <div/>
            )
        } else {
            return (
                <Container>
                    <Card className="m-4">
                        <CardBody>
                            <InputGroup>
                                <InputGroupAddon addonType="prepend">Search</InputGroupAddon>
                                <Input onChange={this.filterPortfolios.bind(this)}/>
                            </InputGroup>
                        </CardBody>
                    </Card>
                    {filteredPortfolios.map(item => (
                        <PortfolioSummary
                            key = {item.p._id}
                            id = {item.p._id}
                            fname = {item.p.properties.fname}
                            lname = {item.p.properties.lname}
                            principal = {item.p.properties.principal}
                        />
                    ))}
                </Container>
            )
        }
    }
}