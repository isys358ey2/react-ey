import React, {Component} from 'react';
import {
    LineChart,
    CartesianGrid,
    XAxis,
    YAxis,
    Tooltip,
    Line,
    ReferenceArea,
    ResponsiveContainer,

} from 'recharts';
import {
    Button,
    CardHeader,
    Dropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Card,
    CardBody,
    Nav,
    NavItem,
    NavLink
} from 'reactstrap';

export default class StockGraph extends Component {
    constructor(props) {
        super(props);

        this.state = {
            portfolio: props.portfolio,
            currentStock: props.portfolio[0]['s.code'],
            selectStockIsOpen: false,
            error: null,
            isLoaded: false,
            data: {},
            left : 'dataMin',
            right : 'dataMax',
            refAreaLeft : '',
            refAreaRight : '',
            top : 'dataMax+1',
            bottom : 'dataMin-1',
            animation : true
        }
    }

    componentWillMount() {
        this.selectStock(this.state.currentStock);
    }

    selectStock(stock) {
        this.setState({
            currentStock: stock,
        });

        fetch(`/api/stock/${this.state.currentStock}/data`).then((response) => {
            return response.json()
        }).then((result) => {
            result.map((x, index) => {
                x['index'] = index;
                x['d.dataDate'] = Date(x['d.dataDate']);
                return x;
            });
            this.setState({
                isLoaded: true,
                data: result,
            })
        }).catch((error) => {
            this.setState({
                error,
                isLoaded: true,
            })
        })
    }

    toggleSelectStock() {
        this.setState({
            selectStockIsOpen: !this.state.selectStockIsOpen,
        })
    }

    getAxisYDomain(from, to, ref, offset) {
        let { data } = this.state;
        const refData = data.slice(from-1, to);
        let [ bottom, top ] = [ refData[0][ref], refData[0][ref] ];
        refData.forEach( d => {
            if ( d[ref] > top ) top = d[ref];
            if ( d[ref] < bottom ) bottom = d[ref];
        });

        return [ (bottom|0) - offset, (top|0) + offset ]
    };

    zoom() {
        let { refAreaLeft, refAreaRight, data } = this.state;

        if ( refAreaLeft === refAreaRight || refAreaRight === '' ) {
            this.setState( () => ({
                refAreaLeft : '',
                refAreaRight : ''
            }) );
            return;
        }

        // xAxis domain
        if ( refAreaLeft > refAreaRight )
            [ refAreaLeft, refAreaRight ] = [ refAreaRight, refAreaLeft ];

        // yAxis domain
        const [ bottom, top ] = this.getAxisYDomain( refAreaLeft, refAreaRight, 'd.price', 1 );

        this.setState( () => ({
            refAreaLeft : '',
            refAreaRight : '',
            data : data.slice(),
            left : refAreaLeft,
            right : refAreaRight,
            bottom,
            top,
        } ) );
    };

    zoomOut() {
        const { data } = this.state;
        this.setState( () => ({
            data : data.slice(),
            refAreaLeft : '',
            refAreaRight : '',
            left : 'dataMin',
            right : 'dataMax',
            top : 'dataMax+1',
            bottom : 'dataMin-1',
        }) );
    }

    render() {
        const { isLoaded, data, left, right, refAreaLeft, refAreaRight, top, bottom, portfolio, selectStockIsOpen } = this.state;

        if (!isLoaded) {
            return (
                <div/>
            )
        } else {
            return (
                <Card className="h-100">
                    <CardHeader>
                        <Nav pills>
                            <NavItem>
                                <NavLink className="m-1">Market View</NavLink>
                            </NavItem>
                            <Dropdown className="m-1" isOpen={selectStockIsOpen} toggle={this.toggleSelectStock.bind(this)}>
                                <DropdownToggle caret>
                                    Select Stock
                                </DropdownToggle>
                                <DropdownMenu>
                                    {
                                        portfolio.map((stock, index) =>
                                            <DropdownItem onClick={this.selectStock.bind(this, stock['s.code'])} key={index}>
                                                {stock['s.code']}
                                            </DropdownItem>
                                        )
                                    }
                                </DropdownMenu>
                            </Dropdown>
                            <NavItem>
                                <Button className="m-1" onClick={this.zoomOut.bind(this)}>Reset Zoom</Button>
                            </NavItem>
                        </Nav>
                    </CardHeader>
                    <CardBody className="h-100">
                        <ResponsiveContainer>
                            <LineChart
                                margin={{ top: 20, right: 30, left: 0, bottom: 0 }}
                                data={data}
                                onMouseDown = { (e) => this.setState({refAreaLeft:e.activeLabel}) }
                                onMouseMove = { (e) => this.state.refAreaLeft && this.setState({refAreaRight:e.activeLabel}) }
                                onMouseUp = { this.zoom.bind( this ) }
                            >
                                <CartesianGrid strokeDasharray="3 3"/>
                                <XAxis
                                    allowDataOverflow={true}
                                    dataKey="index"
                                    domain={[left, right]}
                                    type="number"
                                />
                                <YAxis
                                    allowDataOverflow={true}
                                    domain={[bottom, top]}
                                    type="number"
                                    yAxisId="1"
                                />
                                <Tooltip/>
                                <Line yAxisId="1" type='natural' dataKey='d.price' stroke='#82ca9d' animationDuration={300}/>
                                {
                                    (refAreaLeft && refAreaRight) ? (
                                        <ReferenceArea yAxisId="1" x1={refAreaLeft} x2={refAreaRight}  strokeOpacity={0.3} /> ) : null

                                }
                            </LineChart>
                        </ResponsiveContainer>
                    </CardBody>
                </Card>
            )
        }
    }
}