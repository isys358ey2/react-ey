import React, {Component} from 'react';
import { NavLink } from 'react-router-dom';
import {
    Navbar,
    Nav,
    NavItem,
    NavbarBrand,
    NavbarToggler,
    Collapse,
    UncontrolledDropdown,
    DropdownMenu,
    DropdownToggle,
    DropdownItem
} from 'reactstrap';

export default class Header extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false,
        }
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    isActive = (match, location) => {
        console.log(match);
        console.log(location);
    };

    render() {
        return (
            <div>
                <Navbar color="light" light expand="md">
                    <NavbarBrand>EY Smart Solution</NavbarBrand>
                    <NavbarToggler onClick={this.toggle}/>
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav >
                            <NavItem>
                                <NavLink to="/" className="mx-4" activeClassName="text-muted" isActive={(match, location) => {
                                    return location.pathname === "/";
                                }}>
                                    Dashboard
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink to="/portfolios" className="mx-4" activeClassName="text-muted" isActive={(match, location) => {
                                    return location.pathname.includes("/portfolios");
                                }}>
                                    Manage Portfolios
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink to="/users" className="mx-4" activeClassName="text-muted" isActive={(match, location) => {
                                    return location.pathname === "/users";
                                }}>
                                    Manage Users
                                </NavLink>
                            </NavItem>
                        </Nav>
                        <Nav className="ml-auto">
                            <UncontrolledDropdown nav inNavbar>
                                <DropdownToggle nav caret>
                                    Profile
                                </DropdownToggle>
                                <DropdownMenu right>
                                    <DropdownItem header>First Last</DropdownItem>
                                    <DropdownItem>Edit Profile</DropdownItem>
                                    <DropdownItem>Logout</DropdownItem>
                                </DropdownMenu>
                            </UncontrolledDropdown>
                        </Nav>
                    </Collapse>
                </Navbar>
            </div>
        )
    }
}
