import React, {Component} from 'react';
import {
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    CardText,
    Nav,
    NavItem
} from 'reactstrap';
import { Link } from 'react-router-dom';

export default class PortfolioSummary extends Component {
    render() {
        return (
            <Row className="m-4">
                <Col>
                    <Card>
                        <CardHeader>
                            <Nav pills>
                                <NavItem>
                                    {this.props.fname} {this.props.lname}
                                </NavItem>
                                <NavItem className="ml-auto">
                                    <Link to={"/portfolio/" + this.props.id}>View</Link>
                                </NavItem>
                            </Nav>
                        </CardHeader>
                        <CardBody>
                            <CardText>
                                Principal: {this.props.principal}
                            </CardText>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        )
    }

}