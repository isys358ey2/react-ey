const neo4j = require('neo4j');
const db = new neo4j.GraphDatabase(
    process.env.NEO$J_URL || "http://neo4j:supersecure@localhost:7474"
);

const Stock = module.exports = function Stock(_node) {
    this._node = _node;
};

Stock.getAll = function(callback) {
    const qp = {
        query: [
            'MATCH (s:Stock)',
            'RETURN s.code AS code',
        ].join('\n')
    };

    db.cypher(qp, function(err, result) {
        if (err) return callback(err);
        callback(null, result);
    });
};


Stock.getInfo = function(code, callback) {
    const qp = {
        query: [
            'MATCH (s:Stock)',
            'WHERE s.code = {code}',
            'RETURN s.name, s.code, s.startDate, s.endDate',
        ].join('\n'),
        params: {
            code: code,
        }
    };

    db.cypher(qp, function(err, result) {
        if (err) callback(err);
        callback(null, result);
    })
};

Stock.getData = function(code, callback) {
    const qp = {
        query: [
            'MATCH (s:Stock)-[:hasData]->(d:Data)',
            'WHERE s.code = {code}',
            'RETURN d.dataDate, d.price, d.volume'
        ].join('\n'),
        params: {
            code: code
        }
    };

    db.cypher(qp, function(err,  result) {
        if (err) callback(err);
        callback(null, result);
    });
};



