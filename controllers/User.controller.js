const neo4j = require('neo4j');
const db = new neo4j.GraphDatabase(
    process.env.NEO$J_URL || "http://neo4j:supersecure@localhost:7474"
);
const bcrypt = require('bcrypt-nodejs');

var User = module.exports = function User(_node) {
    this._node = _node;
};

User.get = function(id, callback) {
    const qp = {
        query: [
            'MATCH (user:User)',
            'WHERE ID(user) = {userId}',
            'RETURN user',
        ].join('\n'),
        params: {
            userId: parseInt(id)
        }
    };

    db.cypher(qp, function(err, result) {
        if (err) return callback(err);
        callback(null, result[0]['user']);
    });
};

User.getAll = function (callback) {
    const qp = {
        query: [
            'MATCH (user:User)',
            'RETURN user',
            'LIMIT 100',
        ].join('\n')
    };

    db.cypher(qp, function (err, result) {
        if (err) return callback(err);
        callback(null, result);
    })
};

User.getBy = function (field, value, callback) {
    const qp = {
        query: [
            'MATCH (user:User)',
            'WHERE ' + field + ' = {value}',
            'RETURN user'
        ].join('\n'),
        params: {
            value: value,
        }
    };

    db.cypher(qp, function(err, result) {
        if (err) callback(err);
        if (!result[0]) {
            callback(null, null);
        } else {
            callback(null, result);
        }
    });
};

User.addRelationship = function (relation, userId, otherId, callback) {
};

User.getRelationships = function (id, callback) {
    const qp = {
        query: [
            'START n=node({userId})',
            'MATCH n[r]-(m)',
            'RETURN n, r, m'
        ].join('\n'),
        params: {
            userId: id,
        }
    };

    db.cypher(qp, function (err, result) {
        if (err) callback(err);
        callback(null, result);
    });
};

User.create = function (data, callback) {
    const qp = {
        query: [
            'CREATE (user:User {data})',
            'RETURN user',
        ].join('\n'),
        params: {
            data: data,
        }
    };

    db.cypher(qp, function (err, results) {
        if (err) callback(err);
        callback(null, results);
    });
};

User.update = function (data, callback) {
    const qp = {
        query: [
            'MATCH (user:User)',
            'WHERE id(user) = {userId}',
            'SET user = {props}',
            'RETURN user'
        ].join('\n'),
        params: {
            userId: data.id,
            props: data.props,
        }
    };

    db.cypher(qp, function (err, results) {
        if (err) return callback(err);
        callback(null, results[0]['user']);
    });
};

User.generateHash = function(password, next) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null, next);
};

User.validPassword = function (password, pass, next) {
    return bcrypt.compareSync(password, pass, next);
};
