const neo4j = require('neo4j');
const db = new neo4j.GraphDatabase(
    process.env.NEO$J_URL || "http://neo4j:supersecure@localhost:7474"
);

const Portfolio = module.exports = function Portfolio(_node) {
    this._node = _node;
};

Portfolio.getAll = function(callback) {
    const qp = {
        query: [
            'MATCH (p:Portfolio)',
            'RETURN p',
        ].join('\n')
    };

    db.cypher(qp, function(err, result) {
        if (err) return callback(err);
        callback(null, result);
    });
};

Portfolio.get = function(id, callback) {
    const qp = {
        query: [
            'MATCH (p:Portfolio)-[h:Has]->(s:Stock)',
            'WHERE ID(p) = {id}',
            'RETURN p.fname, p.lname, p.principal, h.amount, s.code, s.name'
        ].join('\n'),
        params: {
            id: parseInt(id),
        }
    };

    db.cypher(qp, function(err, result) {
        if (err) callback(err);
        callback(null, result);
    })
};



