const express = require('express');
const router = express.Router();
const Stock = require('../controllers/Stock.controller');


router.get("/stocks", (req, res) => {
    Stock.getAll((err, result) => {
        if (err) res.status(500).send(err);
        res.send(result);
    })
});

router.get("/stock/:code/info", (req, res) => {
    Stock.getInfo(req.params.code, (err, result) => {
        if (err) res.status(500).send(err);
        res.send(result);
    });
});

router.get("/stock/:code/data", (req, res) => {
    Stock.getData(req.params.code, (err, result) => {
        if (err) res.status(500).send(err);
        res.send(result);
    })
});

module.exports = router;
