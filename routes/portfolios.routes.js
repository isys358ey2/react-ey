const express = require('express');
const router = express.Router();

const Portfolio = require('../controllers/Portfolio.controller');

router.get("/portfolios", (req, res) => {
    Portfolio.getAll((err, result) => {
        if (err) res.status(500).send(err);
        res.send(result);
    })
});

router.get("/portfolio/:id", (req, res) => {
    Portfolio.get(req.params.id, (err, result) => {
        if (err) res.status(500).send(err);
        res.send(result);
    });
});

module.exports = router;



