const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const User = require('../controllers/User.controller');

passport.serializeUser(function(user, done) {
    done(null, user._id);
});

passport.deserializeUser(function (id, done) {
    User.get(id, function (err, user) {
        if (err) return next(err);
        done(err, user);
    });
});

passport.use('local-login', new LocalStrategy({
        passReqToCallback: true,
    },
    function(req, username, password, done) {
        process.nextTick(function() {
            User.getBy('user.email', email, function (err, user) {
                if (err) return done(err);
                if (!user) return done(null, false, req.flash('loginMessage', 'No user found.'));
                if (!User.validPassword(password, user.properties.password))
                    return done(null, false, req.flash('loginMessage', 'Wrong password.'));

                return done(null, user);
            });
        });
    }
));

passport.use('local-signup', new LocalStrategy({
    passReqToCallback: true,
}, function (req, email, password, done) {
    process.nextTick(function () {
        User.getBy('user.email', email, function (err, existingUser) {
            if (err) done(err);
            if (existingUser) return done(null, false, req.flash('loginMessage', 'That email is already in use.'));
            if (req.user) {
                let update = {};
                update.is = req.user._id;
                update.props = {};
                update.props.email = email;
                update.props.password = User.generateHash(password);

                User.update(update, function (err, user) {
                    if (err) throw err;
                    return done(null, user);
                });
            } else {
                let newUser = {};
                newUser.email = email;
                newUser.password = User.generateHash(password);

                User.create(newUser, function (err, user) {
                    if (err) return next(err);
                    return done(null, user);
                });
            }
        });
    });
}));



module.exports = passport;