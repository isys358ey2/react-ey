const express = require('express');
const app = express();
const session = require('express-session');
const flash = require('connect-flash');
const passport = require('./config/configurePassport');

const neo4j = require('neo4j');
const db = new neo4j.GraphDatabase(
    process.env.NEO$J_URL || "http://neo4j:supersecure@localhost:7474"
);
const portfolios = require('./routes/portfolios.routes');
const stocks = require('./routes/stocks.routes');

// Configure login session
app.use(session({ secret: process.env.SESSION_SECRET || "cats!",
                    cookie: { maxAge: 56700000 },
                    resave: false, saveUninitialized: false }));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
app.post('/api/login', passport.authenticate('local', {
    successRedirect: '/', failureRedirect: '/login'}));

// Configure routes
app.use("/api", portfolios);
app.use("/api", stocks);
app.get("/api/ping", (req, res) => {
    res.status(200);
    res.send("Ping!");
});

// Start sever!
app.listen(5000, () => console.log('Server running on port 5000.'));