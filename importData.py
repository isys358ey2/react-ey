import json
import csv
from neo4jrestclient.client import GraphDatabase
import quandl
import random
import os

quandl.ApiConfig.api_key = os.environ["QUANDL_API_KEY"]

data = []
with open("Swiss_investors_final.json", "r") as f:
	data = json.loads(f.read())

stockNames = {}
with open("swiss_stocks.csv", "r") as f:
	reader = csv.reader(f, delimiter=",")
	next(reader, None)
	for row in reader:
		stockNames[row[0]] = {}
		stockNames[row[0]]["name"] = row[1]
		stockNames[row[0]]["quandl"] = row[2]


db = GraphDatabase("http://localhost:7474", username=os.environ["NEO4J_USERNAME"], password=os.environ["NEO4J_PASSWORD"])

# Delete current database
input("Are you sure? This operation will delete the current database. Press Ctrl-C if not.")
db.query("MATCH (n) DETACH DELETE n")

lUser = db.labels.create("User")
lPortfolio = db.labels.create("Portfolio")
lStock = db.labels.create("Stock")

users = {
	"alice": "apples",
	"bob": "banana",
	"carol": "cake",
	"dave": "doodle",
	"eric": "elephant",
	"fred": "fish",
	"greg": "grapes",
}

userNodes = []

for user in users:
	n = db.node(username = user, password = users[user])
	lUser.add(n)
	userNodes.append(n)


stocks = {}

for entity in data:
	p = db.node(fname = entity["Fname"], lname = entity["Lname"], principal=entity["principal"])
	lPortfolio.add(p)
	n = userNodes[random.randint(0, len(userNodes) - 1)]
	n.Manages(p)
	for stock in entity["portfolio"]:
		if stock["stock"] not in stocks:
			stocks[stock["stock"]] = db.node(code = stock["stock"], name = stockNames[stock["stock"]]["name"], quandlCode = stockNames[stock["stock"]]["quandl"])
			lStock.add(stocks[stock["stock"]])
		p.Has(stocks[stock["stock"]], amount=stock["amount"])


for stock in stocks:
	quandlData = quandl.get(stocks[stock].properties['quandlCode'], returns="numpy")
	db.query(f"MATCH (s:Stock {{code: \"{stocks[stock].properties['code']}\"}}) SET s.startDate = date(\"{str(quandlData[0][0])[0:10]}\"), s.endDate = date(\"{str(quandlData[-1][0])[0:10]}\")")
	for row in quandlData:
		db.query(f"MATCH (s:Stock {{code: \"{stocks[stock].properties['code']}\"}}) CREATE (s)-[:hasData]->(n:Data {{dataDate: date(\"{str(row[0])[0:10]}\"), price: {row[1]}, volume: {row[2]}}})")
